unit uTerminalFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Redis, Vcl.StdCtrls;

type
  TfrmTerminal = class(TForm)
    meTerminal: TMemo;
    btSend: TButton;
    edInput: TEdit;
    procedure btSendClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private-Deklarationen }
    fConnection : IRedisConnection;
  public
    { Public-Deklarationen }
    procedure SetConnection(aConnection : IRedisConnection);
  end;

var
  frmTerminal: TfrmTerminal;

implementation

{$R *.dfm}

procedure TfrmTerminal.btSendClick(Sender: TObject);
begin
  meTerminal.Lines.Add('>' + edInput.Text);
  try
  meTerminal.Lines.Add( string( fConnection.ExecuteQuery( AnsiString( edInput.Text))));
  except
    on ex : RedisException do
    begin
      meTerminal.Lines.Add('#' + ex.Message)
    end;

  end;
  edInput.Text := '';
end;

procedure TfrmTerminal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fConnection := nil;
  Action := caFree;
end;

procedure TfrmTerminal.SetConnection(aConnection: IRedisConnection);
begin
  fConnection := aConnection;
end;

end.
